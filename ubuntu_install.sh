#!/bin/bash
if [[ -z "$GITLAB_RUNNER_TOKEN" ]]; then
    echo "Must provide GITLAB_RUNNER_TOKEN in environment" 1>&2
    exit 1
fi

if [[ -z "$GITLAB_RUNNER_DESC" ]]; then
    echo "Must provide GITLAB_RUNNER_DESC in environment" 1>&2
    exit 1
fi

if [[ -z "$USER" ]]; then
    echo "Must provide USER in environment" 1>&2
    exit 1
fi

apt --quiet -y update
apt install -y --no-install-recommends software-properties-common

apt-add-repository --yes ppa:brightbox/ruby-ng
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
apt --quiet -y update

apt install  -y --no-install-recommends \
    openjdk-8-jdk \
    build-essential \
    file \
    lib32stdc++6 \
    lib32z1 \
    tar \
    git \
    gitlab-runner \
    ruby \
    ruby-dev \
    ssh \
    unzip \
    wget \
    zip

# SWITCH TO USER FROM ROOT
su $USER

# GITLAB_RUNNER
RUN gitlab-runner register \
--non-interactive \
--url "https://gitlab.com" \
--registration-token $GITLAB_RUNNER_TOKEN \
--description $GITLAB_RUNNER_DESC \
--executor "shell"

# ANDROID
echo 'export ANDROID_HOME=~/sdks/android' >> ~/.bashrc
echo 'export PATH=${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/tools/bin:${ANDROID_HOME}/platform-tools' >> ~/.bashrc
source ~/.bashrc
mkdir -p $ANDROID_HOME

export ANDROID_COMPILE_SDK="28"
export ANDROID_BUILD_TOOLS="28.0.3"
export ANDROID_SDK_TOOLS="6200805"

wget --quiet --output-document=$ANDROID_HOME/android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip
unzip -d $ANDROID_HOME $ANDROID_HOME/android-sdk.zip
rm -rf $ANDROID_HOME/android-sdk.zip
echo y | $ANDROID_HOME/tools/bin/sdkmanager "platform-tools" "platforms;android-${ANDROID_COMPILE_SDK}" --sdk_root="${ANDROID_HOME}" >/dev/null
echo y | $ANDROID_HOME/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" --sdk_root="${ANDROID_HOME}" >/dev/null
yes | $ANDROID_HOME/tools/bin/sdkmanager  --sdk_root="${ANDROID_HOME}" --licenses

# FLUTTER
echo 'export FLUTTER_HOME=~/sdks/flutter' >> ~/.bashrc
echo 'export PATH=${PATH}:${FLUTTER_HOME}/bin:${FLUTTER_HOME}/bin/cache/dart-sdk/bin' >> ~/.bashrc
source ~/.bashrc
git clone --branch stable https://github.com/flutter/flutter.git ${FLUTTER_HOME}
flutter precache
flutter doctor

# FASTLANE
gem install fastlane
fastlane update_fastlane